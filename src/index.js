/* global Promise */
import electron from 'electron'
import isFunction from 'lodash/isFunction'
import uniqueId from 'lodash/uniqueId'
import merge from 'lodash/merge'

/** Create a string for IPC channel to use for passing message between main and renderer processes
 * @param action The action for which to create the channel sting
 * @param id The id for the dialog$
 */
function channelString(action, id) {
  return `electron-custom-dialog${id ? '_' + id : ''}:${action}`
}

const debug = require('debug')('electron-custom-dialog')
const IS_MAIN = electron.remote == null

const IPC = Object.freeze({
  RESPONSE_TO_RENDERER: 'responseToRenderer',
  OPEN_FROM_RENDERER: 'openFromRenderer',
  RESPONSE: 'response',
  PROPS: 'props'
})

const BROWSERWINDOW_DEFAULTS = {
  alwaysOnTop: true,
  center: true,
  closable: true,
  maximizable: false,
  minimizable: false,
  modal: true,
  resizable: false,
  show: true,
  webPreferences: {
    nodeIntegration: true,
    webgl: false,
    webaudio: false
  }
}

let customDialogs = {}

/** Initialize main-process IPC message listeners for opening dialogs from renderer process.
 */
if (IS_MAIN) {
  const { ipcMain } = electron

  const sendResponseToRenderer = (renderer, response) =>
    renderer.send(channelString(IPC.RESPONSE_TO_RENDERER), response)

  ipcMain.on(channelString(IPC.OPEN_FROM_RENDERER), ({ sender }, remoteName, opt) => {
    debug('openFromRenderer')
    if (!customDialogs[remoteName]) {
      return sendResponseToRenderer(sender, {
        __error__: true,
        __payload__: `electron-custom-dialog: Custom dialog with name "${remoteName}" does not exist!`
      })
    }
    customDialogs[remoteName]
      .open(opt)
      .then(response => sendResponseToRenderer(sender, { __payload__: response }))
      .catch(error =>
        sendResponseToRenderer(sender, { __error__: true, __payload__: error.message || error })
      )
      .catch(
        debug('Sending response to renderer failed. (Probably the renderer was already destroyed)')
      )
  })
}

// internal function for closing a window
function closeWindow(win) {
  if (win && !win.isDestroyed()) {
    if (win.isClosable()) {
      win.close()
    } else {
      win.destroy()
    }
  }
}

/* Create a promise for opening the dialog.
 */
function createOpenPromise(win, prepareOptions, props) {
  const { name, load } = prepareOptions
  const { ipcMain } = electron
  const { webContents, id } = win

  return new Promise((resolve, reject) => {
    let settle = resolve
    win.setAlwaysOnTop(true)
    win.setMenuBarVisibility(false)

    win.on('closed', e => {
      debug('closed', name)
      // Settle the promise after the dialog window has been closed.
      settle()
    })

    webContents.once('did-finish-load', () => {
      debug('finished load', name)
      // Send props to the dialog renderer via IPC
      win.webContents.send(channelString(IPC.PROPS, id), props)

      // Listen to the dialog response
      ipcMain.once(channelString(IPC.RESPONSE, id), (e, response) => {
        debug('response', response)
        settle = () => resolve(response)
        closeWindow(win)
      })
    })
    webContents.once('did-fail-load', (e, errorCode, errorDescription) => {
      debug('failed to load', name, errorCode, errorDescription)
      // A workaround for electron firing this event when baseURLForDataURL-option is given
      // for loadURL, even though did-finish-load is also fired. errorCode 0 means "ok".
      if (errorCode !== 0) {
        settle = () => reject(e, errorCode, errorDescription)
        closeWindow(win)
      }
    })
    webContents.once('crashed', (e, killed) => {
      debug('crashed', name)
      settle = () => reject(e)
      closeWindow(win)
    })

    // Call the load-hook, which should load the window content using loadFile() or loadURL()
    load(win, props)
  })
}

/** Prepare a custom dialog. Custom dialogs need to be prepared in the main-process before you
 * can use them.
 * @param {object} prepareOpt
 * @return {Object} Object providing an interface for the dialog
 * @throws {Error} If prepareOpt is invalid, an error will be thrown
 */
export function prepareDialog(prepareOpt = {}) {
  if (electron.remote) {
    throw Error(
      'electron-custom-dialog: createCustomDialog() called from renderer! ' +
        'Please initialize custom dialogs in the main-process.'
    )
  }

  const { BrowserWindow } = electron
  const { load, name = uniqueId('__electron-custom-dialog_') } = prepareOpt
  const windowOptions = merge({}, BROWSERWINDOW_DEFAULTS, prepareOpt.windowOptions)

  if (customDialogs[name]) {
    throw Error(`electron-custom-dialog: Dialog with name "${name}" already exists!`)
  }

  if (!isFunction(load)) {
    throw Error('electron-custom-dialog: prepareDialog() needs a load()-function!')
  }

  return (customDialogs[name] = {
    /** Asynchronous function for opening the dialog which was initilized
     * @param {object} props Properties, which are passed to the dialog. Use waitProps() from
     * renderer.js to access them in dialog-renderer
     * @return {Promise} Resolved after the dialog window is closed. The response from
     * dialog-renderer can be sent using sendResponse() in renderer.js.
     */
    open(props) {
      // If parent wasn't given, use the focused window
      windowOptions.parent = isFunction(prepareOpt.parent) ? prepareOpt.parent() : prepareOpt.parent

      return createOpenPromise(new BrowserWindow(windowOptions), { load, name }, props)
    }
  })
}

/** Pepare several dialogs at once
 * Dialogs options objects can be given as separate arguments or in an array. See prepareDialog() above for.
 * @return {Array} Array of dialog objects returned by the prepareDialog()
 */
export function prepareDialogs() {
  let dialogs = arguments[0]
  if (!Array.isArray(dialogs)) {
    dialogs = Array.from(arguments)
  }
  return dialogs.map(dialogOpts => prepareDialog(dialogOpts))
}

/** Opens a custom dialog which was initialized  using prepareDialog(). Can be used from main or
 * renderer processes.
 * @param {string} name Name of the custom dialog to open
 * @param {object} opt Options for the dialog renderer
 * @return {Promise} Resolved when the dialog is closed. Rejected if there was an error.
 */
export function openDialog(name, opt) {
  if (electron.remote) {
    const { ipcRenderer } = electron
    return new Promise((resolve, reject) => {
      ipcRenderer.once(channelString(IPC.RESPONSE_TO_RENDERER), (e, res) => {
        res.__error__ ? reject(res.__payload__) : resolve(res.__payload__)
      })
      ipcRenderer.send(channelString(IPC.OPEN_FROM_RENDERER), name, opt)
    })
  } else {
    if (!customDialogs[name]) {
      return Promise.reject(
        Error(`electron-custom-dialog: Dialog with name "${name}" does not exist!`)
      )
    }
    return customDialogs[name].open(opt)
  }
}

/** Send a response from the dialog renderer. This will also close the dialog.
 * @param {*} data The response data. Should be serializable, so it can be sent over IPC.
 **/
export function sendResponse(data) {
  const { remote, ipcRenderer } = electron
  const id = remote.getCurrentWindow().id
  ipcRenderer.send(channelString(IPC.RESPONSE, id), data)
}

/** Get props passed to the dialog renderer using openDialog().
 * @return {Promise} Resolved when the props are available
 */
export function waitProps() {
  const { remote, ipcRenderer } = electron
  const id = remote.getCurrentWindow().id
  return new Promise(resolve => {
    ipcRenderer.once(channelString(IPC.PROPS, id), (e, props) => resolve(props))
  })
}
