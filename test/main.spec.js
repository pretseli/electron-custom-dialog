/* global test */
const fs = require('fs')
const { merge, template } = require('lodash')
const { BrowserWindow, app } = require('electron')
const { prepareDialog, prepareDialogs, openDialog } = require('../dist/index')
const path = require('path')
const { assert } = require('chai')
let debug = require('debug')
debug.enable('electron-custom-dialog:*')

require('./dump-coverage').dumpCoverageOnClose()

// Don't terminate the application when all windows are closed, because it will mess up some tests.
app.on('window-all-closed', e => e.preventDefault())

function initTestDialog(file, opt = {}) {
  return prepareDialog(
    merge(
      {
        load: win => {
          win.webContents.once('did-finish-load', () => {
            setTimeout(() => {
              win.webContents.sendInputEvent({ type: 'mouseUp', x: 100, y: 100, clickCount: 1 })
            }, 200)
          })
          try {
            const url = new URL(file)
            win.loadURL(url)
          } catch (e) {
            win.loadFile(path.resolve(__dirname, file))
          }
        }
      },
      opt
    )
  )
}

test('Should open a dialog with a url', done => {
  const dialog = initTestDialog('simple.html')
  dialog.open({ title: 'Hi!' }).then(() => {
    done()
  })
})

test('Should open a dialog from ejs template.', done => {
  const ejs = fs.readFileSync(path.join(__dirname, 'dialog.ejs'))
  const tmpl = template(ejs.toString())
  const dialog = prepareDialog({
    load(win, opt) {
      win.webContents.on('did-finish-load', () => {
        assert.equal(win.getTitle(), 'ejs dialog')
        setTimeout(() => {
          win.webContents.sendInputEvent({ type: 'mouseUp', x: 100, y: 100, clickCount: 1 })
          done()
        }, 200)
      })
      win.loadURL('data:text/html,' + encodeURIComponent(tmpl(opt)), {
        baseURLForDataURL: `file:///${__dirname}/`
      })
    }
  })

  dialog.open({ title: 'ejs dialog' })
})

test('Should open a dialog with props', done => {
  const dialog = initTestDialog('simple-props.html')
  dialog.open({ title: 'Hi!' }).then(res => {
    assert.equal(res, 'simple-props')
    done()
  })
})

test('Should open dialog using openDialog() from main', done => {
  initTestDialog('simple.html', {
    name: 'simpleDialog'
  })
  openDialog('simpleDialog').then(res => {
    assert.equal(res, 'simple')
    done()
  })
})

test('Should fail loading nonexistent dialog', done => {
  const dialog = initTestDialog('nonexistent.html')
  dialog.open().catch((e, d) => {
    done()
  })
})

test('Should fail when dialog renderer crashes', done => {
  const dialog = prepareDialog({
    load(win) {
      win.loadURL('chrome://crash')
    }
  })
  dialog.open().catch(() => {
    done()
  })
})

test('Should fail when trying to initialize dialog with same name twice', done => {
  initTestDialog('simple.html', {
    name: 'myDialog'
  })

  try {
    initTestDialog('simple.html', {
      name: 'myDialog'
    })
  } catch (e) {
    done()
  }
})

test('Should fail to open nonexistent dialog', done => {
  openDialog('nonExistentDialog').catch(e => {
    done()
  })
})

test('Should fail dialog prepare when no options given', done => {
  try {
    prepareDialog()
  } catch (e) {
    done()
  }
})

test('Should fail to open a dialog with nonexistent HTML from renderer', done => {
  const mainWindow = new BrowserWindow({ width: 800, height: 600 })
  prepareDialogs({
    name: 'customDialogFail',
    parent: mainWindow,
    load(win) {
      win.loadFile(path.resolve(__dirname, 'nonexistent.html'))
    }
  })

  mainWindow.loadFile(path.resolve(__dirname, 'index-fail.html'))
  mainWindow.once('closed', () => {
    done()
  })
})

test('Should fail to open a nonexistent dialog from renderer', done => {
  const mainWindow = new BrowserWindow({ width: 800, height: 600 })
  mainWindow.loadFile(path.resolve(__dirname, 'index-nonexistent.html'))
  mainWindow.once('closed', () => {
    done()
  })
})

test('Should open a dialog from renderer', done => {
  const mainWindow = new BrowserWindow({ width: 800, height: 600 })
  initTestDialog('simple.html', {
    name: 'customDialog',
    parent: mainWindow
  })
  mainWindow.loadFile(path.resolve(__dirname, 'index.html'))
  mainWindow.once('closed', () => {
    done()
  })
})

test('Should open a dialog three times in a row', done => {
  const dialog = initTestDialog('simple.html')
  dialog
    .open()
    .then(() => dialog.open())
    .then(() => dialog.open())
    .then(() => done())
})

test('Should open a dialog prepared with prepareDialogs', done => {
  const dialog = initTestDialog('simple.html')
  dialog
    .open()
    .then(() => dialog.open())
    .then(() => dialog.open())
    .then(() => done())
})
