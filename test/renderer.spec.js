/* global test */
const { prepareDialog } = require('../dist/index')
require('./dump-coverage').dumpCoverageOnClose()

test('Should fail when trying to create dialog from renderer', done => {
  try {
    prepareDialog({
      name: 'failingDialog'
    })
  } catch (e) {
    done()
  }
})
